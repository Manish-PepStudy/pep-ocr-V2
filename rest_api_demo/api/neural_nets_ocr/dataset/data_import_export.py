import boto3
from boto3.session import Session
import os
from botocore.exceptions import ClientError
import logging
import pandas as pd
from rest_api_demo.api.neural_nets_ocr.preprocessing import AlignmentCombined_V2, AlignmentCombined_V3, Alignment_V3_Multithreading, Alignment_V3_Multiprocessing
import time

ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY']
SECRET_KEY = os.environ['AWS_SECRET_KEY']
BUCKET_NAME = os.environ['AWS_S3_BUCKET_DEV']
LOCAL_PATH = '/home/ubuntu/pep-ocr-V2-data/'

log = logging.getLogger()

session = Session(aws_access_key_id = ACCESS_KEY_ID, aws_secret_access_key = SECRET_KEY)
s3 = session.resource('s3')

def local_path_creator(local_dir):
    if not os.path.exists(local_dir):
        os.makedirs(local_dir)

def download_s3_folder(s3_folder, examScheduleId, local_dir=LOCAL_PATH):
    start_time = time.time()
    local_dir = local_dir + s3_folder
    local_path_creator(local_dir)
    log = logging.getLogger()
    count = 0
    start_ind = s3_folder.find('student-omr-sheets')
    s3_folder = s3_folder[start_ind:]

    log.info("Downloading scanned copies...")
    #log.info(s3_folder)
    bucket = s3.Bucket(BUCKET_NAME)
    # for objs in bucket.objects.filter(Prefix=s3_folder):
        # log.info(objs.key)
    for obj in bucket.objects.filter(Prefix=s3_folder):
        # log.info(obj.key)
        target = obj.key if local_dir is None \
            else os.path.join(local_dir, os.path.relpath(obj.key, s3_folder))
        if not os.path.exists(os.path.dirname(target)):
            os.makedirs(os.path.dirname(target))
        if obj.key[-1] == '/':
            continue
        if obj.key.endswith('.jpg'):
            # log.info(f'file in bucket {obj.key}')
            count += 1
            bucket.download_file(obj.key, target)

    log.info(f"Done downloading {len(os.listdir(local_dir))} files.")
    
    dest = AlignmentCombined_V3.alignment(local_dir, examScheduleId, count)
    log.info(f'Done processing {len(os.listdir(dest))} omr sheets in {time.time()-start_time} seconds.')

    


def upload_file(file_path, excel_file_name):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # # Upload the file
    # s3_client = boto3.client('s3', aws_access_key_id=ACCESS_KEY_ID, aws_secret_access_key=SECRET_KEY)
    # try:
    #     for files in os.scandir(file_path):
    #         if files.is_file() and files.path.endswith('.xlsx'):
    #             # print(files.path)
    #             upload_file_key = 'student-omr-sheets/Cycle 2 Test_9/JEEVANI INTERNATIONAL SCHOOL_8/Eighth_3/A_40/Mathematics_3/' + str(files.name)                
    #             s3_client.upload_file(files.path, BUCKET_NAME, upload_file_key)
    # except ClientError as e:
    #     logging.error(e)
    #     return False
    # return True

   # Upload the file
    s3_client = boto3.client('s3', aws_access_key_id=ACCESS_KEY_ID, aws_secret_access_key=SECRET_KEY)
    try:
        start = file_path.find('student-omr-sheets')
        file_path_extracted = file_path[start:]
        upload_file_key = file_path_extracted  
        log.info(f"uploading to file path: {file_path}, bucket: {BUCKET_NAME}, upload file key {upload_file_key}.")
        s3_client.upload_file(file_path, BUCKET_NAME, upload_file_key)
    except ClientError as e:
        logging.error(e)
        return False
    return True
    

if __name__ == "__main__":
    # download_s3_folder(S3_FOLDER)
    response = upload_file('/home/manish/Desktop/jaat2/Excellent School Jaipur_5/Ninth_4/')
    print(response)
