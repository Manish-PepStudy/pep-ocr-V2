import os
import shutil

def remove_directory(dir_path):
    try:
        shutil.rmtree(dir_path, ignore_errors = False)
    except OSError as e:  ## if failed, report it back to the user ##
        print ("Error: %s - %s." % (e.filename, e.strerror))