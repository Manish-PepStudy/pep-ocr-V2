import logging
import time
from flask import request, copy_current_request_context
from flask_restplus import Resource, fields, reqparse
from rest_api_demo.api.neural_nets_ocr.dataset import data_import_export
from rest_api_demo.api.restplus import api
from threading import *
import bottle

log = logging.getLogger(__name__)

ns = api.namespace('omr/scanning', description='Operations related to omr processsing.')

resource_fields = api.model('Resource', {
    'examScheduleId': fields.String,
    'omrPaths': fields.List(fields.String)
})


def process_handle(data):
    start_time = time.time()
    log.info(f'Received data at process: {data}')
    for item in range(len(data)):
        request_data = data[item]
        examScheduleId = request_data['examScheduleId']
        omrPath = request_data['omrPaths']
        for url in omrPath:
            log.info(url)
            data_import_export.download_s3_folder(url, examScheduleId)
        log.info(f'Done processing: {url} in {time.time()-start_time} seconds.')
        

    



# #Immediately return a 200 response to the caller
# return bottle.HTTPResponse(status=200, body="Complete process")

@ns.route('/')
class CategoryCollection(Resource):
    def get(self):
        return "Hello"

    @api.response(201, 'Excel successfully uploaded.')
    @api.expect([resource_fields])
    def post(self):
        """
        Generate excel file for student.
        """
        
        log.info(f'Request received for: {len(request.json)} items.')
        data = request.json
        if not data:
            return bottle.HTTPResponse(status=400, body="Missing data")

        Thread(target=process_handle, args=(data, )).start()

        return True





'''
@ns.route('/')
class CategoryCollection(Resource):

    def get(self):
        return "Hello"

    @api.response(201, 'Excel successfully uploaded.')
    @api.expect([resource_fields])
    def post(self):
        """
        Generate excel file for student.
        """
        log.info(f'request for: {len(request.json)}')
        log.info(request.json)
        for item in range(len(request.json)):
            count=0
            request_data = request.json[item]
            examScheduleId = request_data['examScheduleId']
            omrPath = request_data['omrPaths']
            for url in omrPath:
                log.info(url)
                data_import_export.download_s3_folder(url, examScheduleId)
                break
            break

        return True, 200
'''

# @ns.route('/<int:id>')
# @api.response(404, 'Category not found.')
# class CategoryItem(Resource):

#     @api.marshal_with(category_with_posts)
#     def get(self, id):
#         """
#         Returns a category with a list of posts.
#         """
#         return Category.query.filter(Category.id == id).one()

#     @api.expect(category)
#     @api.response(204, 'Category successfully updated.')
#     def put(self, id):
#         """
#         Updates a blog category.

#         Use this method to change the name of a blog category.

#         * Send a JSON object with the new name in the request body.

#         ```
#         {
#           "name": "New Category Name"
#         }
#         ```

#         * Specify the ID of the category to modify in the request URL path.
#         """
#         data = request.json
#         update_category(id, data)
#         return None, 204

#     @api.response(204, 'Category successfully deleted.')
#     def delete(self, id):
#         """
#         Deletes blog category.
#         """
#         delete_category(id)
#         return None, 204
