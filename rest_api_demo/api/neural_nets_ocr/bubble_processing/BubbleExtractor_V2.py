import cv2 as cv
import numpy as np
import pandas as pd
import time
import os
import logging
from rest_api_demo.api.neural_nets_ocr.dataset import data_import_export
from rest_api_demo.api.neural_nets_ocr.ocr_engine import get_sheets_data_V2

start_time = time.time()
lengi = 0

# Coordinates for Question boxes and setid and studentid
xForQuads = [(265, 467), (585, 787), (905, 1107), (1223, 1425)]
yForQuads = [(986, 1176), (1204, 1395), (1422, 1610), (1637, 1830), (1856, 2042)]

# y1, y2, x1, x2
setCords = [555, 920, 770, 959]
studentCords = [555, 920, 1003, 1421]

# Threshold to determine what percent of area of bubbles be colored to consider them marked.
thresholdForWhitePixels = 0.195

# This module reads bubbles marked for answering.
def getAnswerKeys(img):
    imageOriginal = img

    allScore = []
    answer_keys = ''
    for vert in xForQuads:
        for hor in yForQuads:
            image = imageOriginal[hor[0]:hor[1], vert[0]:vert[1]]
            # <<<<<<<<<<<<<<<<---------------- Preprocessing and removing channels ---------->>>>>>>>>>
            h, w = image.shape[1], image.shape[0]
            image = image[20:h-10, :]
            rows = np.array_split(image, 5, axis=0) #<<<<<--------- Split image into 5 rows ----------->>>>>>

            r, a, b, c = 1, 0.25, 0.5, 1
            localKernel = np.array([[a, a, a, a, a, a],
                                    [a, b, b, b, b, a],
                                    [a, b, c, c, b, a],
                                    [a, b, c, c, b, a],
                                    [a, b, b, b, b, a],
                                    [a, a, a, a, a, a]], dtype='float16')
            for row in rows:

                cols = np.array_split(row, 4, axis=1)  #<<<<<<<<<<<<<<<<-------------------- Splits each row into 4 Cells

                tempList = []
                empty = True
                for col in cols:

                    col = cv.resize(col, (36, 36), interpolation=cv.INTER_LINEAR)
                    hCol, wCol = col.shape[1], col.shape[0]
                    center = [int(hCol / 2), int(wCol / 2)]
                    lowerH, upperH, lowerW, upperW = center[0] - 18, center[0] + 18, center[1] - 18, center[0] + 18
                    col = col[lowerH:upperH, lowerW:upperW]

                    ################################ Internal division of a cell in 6x6 #########################
                    colMatrix = np.zeros((6, 6), dtype='float16')
                    colRowGrids = np.array_split(col, 6, axis=0)   #<<<<<<---------------- Splits each cell into 6 rows
                    i = 0

                    for colRowGrid in colRowGrids:
                        totalNzcValue = 36
                        colGrids = np.array_split(colRowGrid, 6, axis=1) #<<<<<<---------------- Splits each cell row into 6 smaller cells of size 6x6
                        j = 0
                        if totalNzcValue != 0:
                            for colGrid in colGrids:
                                nzc = np.count_nonzero(colGrid)
                                colMatrix[i][j] = float(nzc/totalNzcValue)
                                j += 1
                        i += 1
                    aggVal = np.round(np.sum(colMatrix*localKernel)/np.sum(localKernel), 4)
                    allScore.append(aggVal)
                    tempList.append(aggVal)

                no_of_bubbles_marked = 0
                for index in range(len(tempList)):
                    if tempList[index] > thresholdForWhitePixels:
                        no_of_bubbles_marked += 1


                if no_of_bubbles_marked > 1:
                    empty = True
                else:
                    for index in range(len(tempList)):
                        if tempList[index] > thresholdForWhitePixels:
                            empty = False
                            answer_keys += str(index + 1)

                if empty == True:
                    answer_keys += str('x')
                answer_keys += '#'
    return answer_keys

# This moule reads bubbles for student id and set id.
def getIds(image, num):
    allScore = []
    reqString = ''
    r, a, b, c = 1, 0.25, 0.5, 1
    localKernel = np.array([[a, a, a, a, a, a],
                            [a, b, b, b, b, a],
                            [a, b, c, c, b, a],
                            [a, b, c, c, b, a],
                            [a, b, b, b, b, a],
                            [a, a, a, a, a, a]], dtype='float16')

    rows = np.array_split(image, num, axis=1)  # <<<<<<<<<<<<<----------------- Split image into num cols ------------------------>>>>>>>>>>>

    for row in rows:
        row = cv.resize(row, (44, 360), interpolation=cv.INTER_AREA)
        cols = np.array_split(row, 10, axis=0)  # <<<<<<<<<<<<<<<<-------------------- Splits each row into 4 Cells


        tempList = []
        empty = True
        for col in cols:
            col = cv.resize(col, (36, 36), interpolation=cv.INTER_LINEAR)
            hCol, wCol = col.shape[1], col.shape[0]
            center = [int(hCol / 2), int(wCol / 2)]
            lowerH, upperH, lowerW, upperW = center[0] - 18, center[0] + 18, center[1] - 18, center[0] + 18
            col = col[lowerH:upperH, lowerW:upperW]


            ################################ Internal division of a cell in 6x6 #########################
            colMatrix = np.zeros((6, 6), dtype='float16')
            colRowGrids = np.array_split(col, 6, axis=0)  # <<<<<<---------------- Splits each cell into 6 rows
            i = 0

            for colRowGrid in colRowGrids:
                totalNzcValue = 36
                colGrids = np.array_split(colRowGrid, 6,
                                          axis=1)  # <<<<<<---------------- Splits each cell row into 6 smaller cells of size 6x6
                j = 0
                if totalNzcValue != 0:
                    for colGrid in colGrids:
                        nzc = np.count_nonzero(colGrid)
                        colMatrix[i][j] = float(nzc / totalNzcValue)
                        j += 1
                i += 1
            aggVal = np.round(np.sum(colMatrix * localKernel) / np.sum(localKernel), 4)
            allScore.append(aggVal)
            tempList.append(aggVal)

        blank = True
        no_of_bubbles_marked_id = 0
        for index in range(len(tempList)):
            if tempList[index] > thresholdForWhitePixels:
                no_of_bubbles_marked_id += 1

        if no_of_bubbles_marked_id > 1:
            blank = True
        else:
            for index in range(len(tempList)):
                if tempList[index] > thresholdForWhitePixels:
                    blank = False
                    reqString += str(index)

        if blank == True:
            reqString += 'x'

    if reqString == '':
        if num == 9:
            reqString = 'xxxxxxxxx'
        else:
            reqString = 'xxxx'
    return reqString

# This module removes channels from given image. 
def image_thresholding(imageOriginal):
    imageOriginal = imageOriginal.copy()
    imageHSV = cv.cvtColor(imageOriginal, cv.COLOR_BGR2HSV)
    lBound = (0, 0, 0)
    uBound = (255, 255, 140)
    mask = cv.inRange(imageHSV, lBound, uBound)
    mask_not = cv.bitwise_not(mask)
    imageWithoutChannels = cv.bitwise_and(imageOriginal, imageOriginal, mask=mask_not)
    imageGray = cv.cvtColor(imageWithoutChannels, cv.COLOR_BGR2GRAY)
    _, imageThresh = cv.threshold(imageGray, 80, 255, cv.THRESH_BINARY_INV)

    return imageThresh

def extract_bubbles(img_dir, examScheduleId, roi_dir):
    df = pd.DataFrame(columns=['image_name', 'set_id', 'set_id_bubble', 'student_id', 'answer_keys'])
    lengi = 0
    log = logging.getLogger()
    log.info(f'Bubble processing is started...')
    for entry in os.scandir(img_dir):
        if entry.path.endswith('.jpg') and entry.is_file():
            src = entry.path
            start = src.find('image')
            end = src.find('.jpg')
            docId = src[start:]
            imageOriginal = cv.imread(src, -1)

            imageThresh = image_thresholding(imageOriginal)

            imageSet = imageThresh[setCords[0]:setCords[1], setCords[2]:setCords[3]]
            imageStudent = imageThresh[studentCords[0]:studentCords[1], studentCords[2]:studentCords[3]]
            originalSetId = getIds(imageSet, 4)
            data = [docId, str(examScheduleId), originalSetId, getIds(imageStudent, 9), getAnswerKeys(imageThresh)]
            '''
            requiredSetId_check = originalSetId.find(str(examScheduleId))
            if requiredSetId_check != -1:
                data = [docId, str(examScheduleId), originalSetId, getIds(imageStudent, 9), getAnswerKeys(imageThresh)]
            else:
                data = [docId, str(examScheduleId), originalSetId, getIds(imageStudent, 9), getAnswerKeys(imageThresh)]
            '''
             
            df.loc[len(df.index)] = data
            lengi += 1

    log.info(f'Done processing bubbles in {lengi} sheets.')
    get_sheets_data_V2.predict_student_details(roi_dir, examScheduleId, df)
    del df

if __name__ == '__main__':
    extract_bubbles('/home/manish/Desktop/NewImgDir/', 5,'/home/manish/Desktop/jaat3/roi/')
    print(f'Total time taken: {(time.time() - start_time) / float(60)} minutes.')
