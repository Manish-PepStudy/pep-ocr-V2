import cv2 as cv
import cv2
import numpy as np
import pandas as pd
import time
import os
import logging
from rest_api_demo.api.neural_nets_ocr.dataset import data_import_export
from rest_api_demo.api.neural_nets_ocr.ocr_engine import get_sheets_data_V2

start_time = time.time()
lengi = 0

# Coordinates for Question boxes and setid and studentid
xForQuads = [(265, 467), (585, 787), (905, 1107), (1223, 1425)]
yForQuads = [(986, 1176), (1204, 1395), (1422, 1610), (1637, 1830), (1856, 2042)]

# y1, y2, x1, x2
setCords = [555, 920, 770, 959]
studentCords = [555, 920, 1003, 1421]

# Threshold to determine what percent of area of bubbles be colored to consider them marked.
thresholdForWhitePixels = 0.195

# This module reads bubbles marked for answering.
def getAnswerKeys(img):
    imageOriginal = img

    allScore = []
    answer_keys = ''
    totalCellCount = 36
    for vert in xForQuads:
        for hor in yForQuads:
            image = imageOriginal[hor[0]:hor[1], vert[0]:vert[1]]

            quesGroupHeight, quesGroupWidth, _ = image.shape

            # ---------------- Preprocessing image for bubble detection ----------

            imageGray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
            _, imageThresh = cv.threshold(imageGray, 80, 255, cv.THRESH_BINARY_INV)

            # Remove noise from the image
            imageBlur = cv2.GaussianBlur(imageGray, (5,5), 0)

            # Detect edges in the image
            edges = cv2.Canny(imageBlur, threshold1=30, threshold2=100)

            # Morphological operations
            strucElementKernal = cv2.getStructuringElement(cv2.MORPH_RECT, (35,1))
            closing = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, strucElementKernal)

            
            # Contours and bounding boxes
            contours, heirarchy = cv2.findContours(closing, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)

            bounding_boxes = [cv2.boundingRect(contour) for contour in contours]
            boundaryYAxes = [cordinates[1] for cordinates in bounding_boxes if cordinates[3]>= 20]
            
            if len(boundaryYAxes) == 0:
                break
                
            upperLeftY = min(boundaryYAxes)
            lowerLeftY = max(boundaryYAxes)

            bubbleHeight = [x[3] for x in bounding_boxes]
            # lowerLeftY += max(bubbleHeight)
            lowerLeftY += max(bubbleHeight)

            if lowerLeftY > quesGroupHeight:
              lowerLeftY = quesGroupHeight
              print("Warning: Something went wrong with contour for dividing questions group into rows")

            print("Warning: Something went wrong with contour for dividing after")
            imageProcessed = imageThresh[upperLeftY:lowerLeftY, :]
         
            # ----------------- Starting marked bubble detection ---------------


            # --------- Splits image into 5 rows -----------
            rows = np.array_split(imageProcessed, 5, axis=0) 

            r, a, b, c = 1, 0.25, 0.5, 1
            localKernel = np.array([[a, a, a, a, a, a],
                                    [a, b, b, b, b, a],
                                    [a, b, c, c, b, a],
                                    [a, b, c, c, b, a],
                                    [a, b, b, b, b, a],
                                    [a, a, a, a, a, a]], dtype='float16')
            for row in rows:

                # -------- Splits each row into 4 Cells -------
                cols = np.array_split(row, 4, axis=1)  

                tempList = []
                empty = True
                for col in cols:
                    
                    col = cv.resize(col, (36, 36), interpolation=cv.INTER_LINEAR)
                    hCol, wCol = col.shape[1], col.shape[0]
                    center = [int(hCol / 2), int(wCol / 2)]
                    lowerH, upperH, lowerW, upperW = center[0] - 18, center[0] + 18, center[1] - 18, center[0] + 18
                    col = col[lowerH:upperH, lowerW:upperW]

                    # ------ Division of a cells into 6x6 grids ---------
                    colMatrix = np.zeros((6, 6), dtype='float16')

                    # ------- Splits each cell into 6 rows -----------
                    colRowGrids = np.array_split(col, 6, axis=0)   
                    i = 0

                    for colRowGrid in colRowGrids:
                      # ------ Splits each cell row into 6 smaller cells of size 6x6
                      colGrids = np.array_split(colRowGrid, 6, axis=1) 

                      j = 0
                      for colGrid in colGrids:
                        nzc = np.count_nonzero(colGrid)
                        colMatrix[i][j] = float(nzc/totalCellCount)
                        j += 1
                      i += 1
                    aggVal = np.round(np.sum(colMatrix*localKernel)/np.sum(localKernel), 4)
                    allScore.append(aggVal)
                    tempList.append(aggVal)

                no_of_bubbles_marked = 0
                for index in range(len(tempList)):
                    if tempList[index] > thresholdForWhitePixels:
                        no_of_bubbles_marked += 1


                if no_of_bubbles_marked > 5:
                    empty = True
                else:
                    for index in range(len(tempList)):
                        if tempList[index] > thresholdForWhitePixels:
                            empty = False
                            answer_keys += str(index + 1)

                if empty == True:
                    answer_keys += str('x')
                answer_keys += '#'
    return answer_keys

# This moule reads bubbles for student id and set id.
def getIds(image, num):
    allScore = []
    reqString = ''
    r, a, b, c = 1, 0.25, 0.5, 1
    localKernel = np.array([[a, a, a, a, a, a],
                            [a, b, b, b, b, a],
                            [a, b, c, c, b, a],
                            [a, b, c, c, b, a],
                            [a, b, b, b, b, a],
                            [a, a, a, a, a, a]], dtype='float16')

    rows = np.array_split(image, num, axis=1)  # <<<<<<<<<<<<<----------------- Split image into num cols ------------------------>>>>>>>>>>>

    for row in rows:
        row = cv.resize(row, (44, 360), interpolation=cv.INTER_AREA)
        cols = np.array_split(row, 10, axis=0)  # <<<<<<<<<<<<<<<<-------------------- Splits each row into 4 Cells


        tempList = []
        empty = True
        for col in cols:
            col = cv.resize(col, (36, 36), interpolation=cv.INTER_LINEAR)
            hCol, wCol = col.shape[1], col.shape[0]
            center = [int(hCol / 2), int(wCol / 2)]
            lowerH, upperH, lowerW, upperW = center[0] - 18, center[0] + 18, center[1] - 18, center[0] + 18
            col = col[lowerH:upperH, lowerW:upperW]


            ################################ Internal division of a cell in 6x6 #########################
            colMatrix = np.zeros((6, 6), dtype='float16')
            colRowGrids = np.array_split(col, 6, axis=0)  # <<<<<<---------------- Splits each cell into 6 rows
            i = 0

            for colRowGrid in colRowGrids:
                totalNzcValue = 36
                colGrids = np.array_split(colRowGrid, 6,
                                          axis=1)  # <<<<<<---------------- Splits each cell row into 6 smaller cells of size 6x6
                j = 0
                if totalNzcValue != 0:
                    for colGrid in colGrids:
                        nzc = np.count_nonzero(colGrid)
                        colMatrix[i][j] = float(nzc / totalNzcValue)
                        j += 1
                i += 1
            aggVal = np.round(np.sum(colMatrix * localKernel) / np.sum(localKernel), 4)
            allScore.append(aggVal)
            tempList.append(aggVal)

        blank = True
        no_of_bubbles_marked_id = 0
        for index in range(len(tempList)):
            if tempList[index] > thresholdForWhitePixels:
                no_of_bubbles_marked_id += 1

        if no_of_bubbles_marked_id > 1:
            blank = True
        else:
            for index in range(len(tempList)):
                if tempList[index] > thresholdForWhitePixels:
                    blank = False
                    reqString += str(index)

        if blank == True:
            reqString += 'x'

    if reqString == '':
        if num == 9:
            reqString = 'xxxxxxxxx'
        else:
            reqString = 'xxxx'
    return reqString

# This module removes channels from given image. 
def image_thresholding(imageOriginal):
    imageOriginal = imageOriginal.copy()
    imageHSV = cv.cvtColor(imageOriginal, cv.COLOR_BGR2HSV)
    lBound = (0, 0, 0)
    uBound = (255, 255, 140)
    mask = cv.inRange(imageHSV, lBound, uBound)
    mask_not = cv.bitwise_not(mask)
    imageWithoutChannels = cv.bitwise_and(imageOriginal, imageOriginal, mask=mask_not)
    imageGray = cv.cvtColor(imageWithoutChannels, cv.COLOR_BGR2GRAY)
    _, imageThresh = cv.threshold(imageGray, 80, 255, cv.THRESH_BINARY_INV)

    return imageThresh

def extract_bubbles(img_dir, examScheduleId, roi_dir):
    df = pd.DataFrame(columns=['image_name', 'set_id', 'set_id_bubble', 'student_id', 'answer_keys'])
    lengi = 0
    log = logging.getLogger()
    log.info(f'Bubble processing is started for {img_dir}...')
    for entry in os.scandir(img_dir):
        if entry.path.endswith('.jpg') and entry.is_file():
            src = entry.path
            docId = os.path.basename(src)
            imageOriginal = cv.imread(src, -1)

            log.info(f'Bubble processing is started for {docId}...')
            imageThresh = image_thresholding(imageOriginal)

            imageSet = imageThresh[setCords[0]:setCords[1], setCords[2]:setCords[3]]
            imageStudent = imageThresh[studentCords[0]:studentCords[1], studentCords[2]:studentCords[3]]
            originalSetId = getIds(imageSet, 4)
            log.info(f'Bubble processing is started for {docId}...')
            data = [docId, str(examScheduleId), originalSetId, getIds(imageStudent, 9), getAnswerKeys(imageOriginal)]
            '''
            requiredSetId_check = originalSetId.find(str(examScheduleId))
            if requiredSetId_check != -1:
                data = [docId, str(examScheduleId), originalSetId, getIds(imageStudent, 9), getAnswerKeys(imageThresh)]
            else:
                data = [docId, str(examScheduleId), originalSetId, getIds(imageStudent, 9), getAnswerKeys(imageThresh)]
            '''
            log.info(f'Bubble processing is completed for {docId}...')
            df.loc[len(df.index)] = data
            lengi += 1

    log.info(f'Done processing bubbles for {lengi} sheets.')
    get_sheets_data_V2.predict_student_details(roi_dir, examScheduleId, df)
    del df

if __name__ == '__main__':
    extract_bubbles('/home/manish/Desktop/NewImgDir/', 5,'/home/manish/Desktop/jaat3/roi/')
    print(f'Total time taken: {(time.time() - start_time) / float(60)} minutes.')
