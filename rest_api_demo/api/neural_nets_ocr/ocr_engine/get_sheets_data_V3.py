import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras.models import load_model
import os
import time
import pickle
from scipy.stats import entropy
import statistics as stat
import logging
from rest_api_demo.api.neural_nets_ocr.dataset import data_import_export
from rest_api_demo.api.neural_nets_ocr import directory_eraser

start_time = time.time()

############### ROI Coordinates ##############
w = 50
h = 58
uY = 55
cords = [22, 70, 117, 165, 255, 305, 352, 394, 442, 487, 534, 580, 627]

def error(row, examScheduleId):
  val = ''
  if (str(row['student_id']) != str(row['student_id_ocr'])) and (str(row['set_id']) != str(row['set_id_ocr'])):
    val = 'STUDENT,SET'
  elif (str(row['student_id']) != str(row['student_id_ocr'])) and (str(row['set_id']) == str(row['set_id_ocr'])):
    val = 'STUDENT'
  elif (str(row['student_id']) == str(row['student_id_ocr'])) and (str(row['set_id']) != str(row['set_id_ocr'])):
    val = 'SET'
  else:
    val = ''

  if (str(row['set_id'])!=str(examScheduleId)):
    if val == '':
      val = 'SET'
    if val == 'STUDENT':
      val = val + ',SET'

  return val

def predict_student_details(img_dir, examScheduleId, bubble_dataframe=None):
  log = logging.getLogger()
  itt = 0
  ocr_dataframe = pd.DataFrame(columns=['set_id_ocr', 'confidence_set_id_ocr', 'student_id_ocr', 'confidence_student_id_ocr'])
  student_with_docId = {} #Contaings student_id having key value as document_id

  #Load trained Models
  model = load_model('/content/drive/MyDrive/OCR_Related/ocr_V3')

  modelsEnsem = [model]

  for entry in os.scandir(img_dir):
    if entry.path.endswith('.jpg') and entry.is_file():
      src = entry.path
      start = src.find('image')+5
      end = src.find('.jpg')
      docId = src[start:end]

      imageOrig = cv2.imread(src, -1)
      ret, thresh = cv2.threshold(imageOrig, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
      cv2_imshow(thresh)
      studentIdString = ""
      setIdString = ""
      stripList = [] #Contains images with corresponding predictions
      confList = []

      kernel = np.ones((3,3), np.uint8)

      for i in range(0, 13):
        roi = thresh[uY:uY+h, cords[i]:cords[i]+w]
        # roi = cv2.dilate(roi, kernel, iterations=1)
        roi = cv2.resize(roi, (28,28), interpolation=cv2.INTER_AREA)
        inDigit = roi.reshape(-1, 28,28,1)
        inDigit = inDigit/255

        results = modelsEnsem[0].predict(inDigit)
        
        confList.append(float("{:.2f}".format((1 - entropy(results[0], base=2))*100)))
        results = np.argmax(results,axis = 1)
        stripList.append([roi, results[0], [cords[i], uY, w, h]])

        if i <=3:
          if len(str(results[0])) == 2:
            setIdString += 'x'
          else:
            setIdString += str(results[0])
        else:
          if len(str(results[0])) == 2:
            studentIdString += 'x'
          else:
            studentIdString += str(results[0])

      student_with_docId[str(docId)] = [setIdString, studentIdString] 
      ocr_dataframe.loc[len(ocr_dataframe.index)] = [setIdString, float("{:.2f}".format(stat.mean(confList[:2]))), studentIdString, float("{:.2f}".format(stat.mean(confList[2:])))]

      itt += 1

  log.info(f"--- Processed {itt} files in {((time.time() - start_time)/60)} minutes ---")
  final_dataframe = pd.concat([bubble_dataframe, ocr_dataframe], axis=1)
  
  excel_file_path = os.path.normpath(img_dir+os.sep+os.pardir)
  excel_file_name1 = '/ml_result_'+str(examScheduleId)+'.xlsx'
  excel_file_name2 = '/result_'+str(examScheduleId)+'.xlsx'

  final_dataframe['error'] = final_dataframe.apply(error, axis=1, result_type='reduce', examScheduleId=examScheduleId)
  final_dataframe['error_history'] = final_dataframe.error
  final_dataframe.to_excel(str(excel_file_path + excel_file_name1), index=False)
  final_dataframe.to_excel(str(excel_file_path + excel_file_name2), index=False)
  del bubble_dataframe
  del ocr_dataframe
  del final_dataframe

  log.info(f'Successfully generated excel file.')

  response_ml_result = data_import_export.upload_file(str(excel_file_path+excel_file_name1), excel_file_name1)
  response_result = data_import_export.upload_file(str(excel_file_path+excel_file_name2), excel_file_name2)
  # par_dir = os.path.normpath(excel_file_path+os.sep+os.pardir)
  if response_ml_result == True and response_result == True:
      log.info('Excel file successfully uploaded to s3.')
      # directory_eraser.remove_directory(excel_file_path)
  else:
      log.info('Failed to upload excel file to s3.')
