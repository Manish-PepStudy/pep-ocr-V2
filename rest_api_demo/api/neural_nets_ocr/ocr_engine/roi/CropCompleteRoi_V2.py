import cv2 as cv
import numpy as np
import os
import re

# Coordinates for complete roi in new omr design according to St. Joseph Academy.
# For English OMR

ulcorn = [747, 446]
lrcorn = [1453, 950]

# # For Hindi OMR
# ulcorn = [623, 370]
# lrcorn = [1453, 950]

def crop_roi(img_dir):
    dest = os.path.normpath(img_dir+os.sep+os.pardir)+"/"+"roi/"
    if not os.path.exists(dest):
        os.makedirs(dest)
    for entry in os.scandir(img_dir):
        if entry.path.endswith('.jpg') and entry.is_file():
            src = entry.path
            start = src.find('image')
            end = src.find('.jpg')
            doc_id = os.path.basename(src)

            image = cv.imread(src, -1)

            studRoi = image[ulcorn[1]:lrcorn[1], ulcorn[0]:lrcorn[0]]

            savePath = dest + doc_id
            cv.imwrite(savePath, studRoi)
    return True, dest

if __name__ == '__main__':
    crop_roi('/content/drive/MyDrive/OCR_Related/SampleProjectData/St. Joseph School_30/Tenth_5/A_191/Mathematics_15/aligned')
