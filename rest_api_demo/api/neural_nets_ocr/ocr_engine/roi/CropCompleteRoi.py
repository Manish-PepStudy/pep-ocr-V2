import cv2 as cv
import numpy as np
import os
import re
import logging

# Coordinates for complete roi in old omr design.
# ulcorn = [821, 374]
# lrcorn = [1403, 877]

# Coordinates for complete roi in new omr design.
ulcorn = [762, 475]
lrcorn = [1417, 975]

def crop_roi(img_dir):
    log = logging.getLogger()
    dest = os.path.normpath(img_dir+os.sep+os.pardir)+"/"+"roi/"
    log.info(dest)
    if not os.path.exists(dest):
        os.makedirs(dest)
    for entry in os.scandir(img_dir):
        if entry.path.endswith('.jpg') and entry.is_file():
            src = entry.path
            start = src.find('image')
            end = src.find('.jpg')
            doc_id = src[start:]

            image = cv.imread(src, -1)

            studRoi = image[ulcorn[1]:lrcorn[1], ulcorn[0]:lrcorn[0]]

            savePath = dest + doc_id
            cv.imwrite(savePath, studRoi)
    return True, dest

if __name__ == '__main__':
    crop_roi('/home/manish/Desktop/jaat3/aligned/')
