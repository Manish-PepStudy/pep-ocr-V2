import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras.models import load_model
import os
import time
import pickle
from scipy.stats import entropy
import statistics as stat
import logging
from rest_api_demo.api.neural_nets_ocr.dataset import data_import_export
from rest_api_demo.api.neural_nets_ocr import directory_eraser

start_time = time.time()

############### Student details related ROI Coordinates on the basis of St. Joseph Academy ##############
w = 46
h = 55
uY = 57
cords = [22, 69, 117, 165, 255, 303, 348, 395, 441, 486, 533, 580, 624]

# This method consider error in set id when set_id is not examScheduleId
def error(row, examScheduleId):
  val = ''
  # print(f'*************** Exam Schedule Id: {examScheduleId} *******************') 
  if (str(row['student_id']) != str(row['student_id_ocr'])) and (str(row['set_id_bubble']) != str(row['set_id_ocr'])):
    val = 'STUDENT'
  elif (str(row['student_id']) != str(row['student_id_ocr'])) and (str(row['set_id_bubble']) == str(row['set_id_ocr'])):
    val = 'STUDENT'
  elif (str(row['student_id']) == str(row['student_id_ocr'])) and (str(row['set_id_bubble']) != str(row['set_id_ocr'])):
    val = ''
  else:
    val = ''

  # print(f'***************** Val : {val} ****************')
  # print(row['set_id'])
  # print(f'***********************{str(examScheduleId)}, {row['set_id']}*********************')
  if (str(row['set_id'])!=str(examScheduleId)):
    # print('*************** inside target ***************')
    if val == '':
      val = 'SET'
    if val == 'STUDENT':
      val = val + ',SET'

  return val

# This method ignores errors in set id field completely and used in results.xlsx .
def error_processed_result_file(row, examScheduleId):
  val = ''
  # print(f'*************** Exam Schedule Id: {examScheduleId} *******************') 
  if ( str(row['student_id']) != str(row['student_id_ocr']) ):
    val = 'STUDENT'
  else:
    val = ''
  return val


# This module removes channels from given image. 
def image_thresholding(imageOriginal):
    imageOriginal = imageOriginal.copy()
    imageHSV = cv2.cvtColor(imageOriginal, cv2.COLOR_BGR2HSV)
    lBound = (0, 0, 0)
    uBound = (255, 255, 190)
    mask = cv2.inRange(imageHSV, lBound, uBound)
    mask_not = cv2.bitwise_not(mask)
    imageWithoutChannels = cv2.bitwise_and(imageOriginal, imageOriginal, mask=mask)
    imageWithoutChannels[np.where((imageWithoutChannels == [0, 0, 0]).all(axis=2))] = [255, 255, 255]
    imageGray = cv2.cvtColor(imageWithoutChannels, cv2.COLOR_BGR2GRAY)

    return imageGray

def predict_student_details(img_dir, examScheduleId, bubble_dataframe):
  log = logging.getLogger()
  itt = 0
  ocr_dataframe = pd.DataFrame(columns=['set_id_ocr', 'confidence_set_id_ocr', 'student_id_ocr', 'confidence_student_id_ocr', 'ignore'])
  student_with_docId = {} #Contaings student_id having key value as document_id

  #Load trained Models
  log.info(f'Loading model for ocr...')
  model = load_model('/home/ubuntu/pep-ocr-V2/rest_api_demo/api/neural_nets_ocr/ocr_engine/models/ocr_V4')
  log.info(f'Ocr model loaded.')

  modelsEnsem = [model]

  for entry in os.scandir(img_dir):
    if entry.path.endswith('.jpg') and entry.is_file():
      src = entry.path
      start = src.find('image')+5
      end = src.find('.jpg')
      docId = os.path.basename(src)
      log.info(f'Processing {docId} for ocr...')
      imageOrig = cv2.imread(src, -1)
      image = image_thresholding(imageOrig)
      # cv2_imshow(image)
      ret, thresh = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)

      studentIdString = ""
      setIdString = ""
      stripList = [] #Contains images with corresponding predictions
      confList = []

      for i in range(0, 13):
        roi = thresh[uY:uY+h, cords[i]:cords[i]+w]
        roi = cv2.resize(roi, (28,28), interpolation=cv2.INTER_AREA)
        inDigit = roi.reshape(-1, 28,28,1)
        # cv2_imshow(roi)
        inDigit = inDigit/255

        results = modelsEnsem[0].predict(inDigit)
        
        confList.append(float("{:.2f}".format((1 - entropy(results[0], base=2))*100)))
        results = np.argmax(results,axis = 1)
        stripList.append([roi, results[0], [cords[i], uY, w, h]])

        if i <=3:
          if len(str(results[0])) == 2:
            setIdString += 'x'
          else:
            setIdString += str(results[0])
        else:
          if len(str(results[0])) == 2:
            studentIdString += 'x'
          else:
            studentIdString += str(results[0])

      student_with_docId[str(docId)] = [setIdString, studentIdString]  
      ocr_dataframe.loc[len(ocr_dataframe.index)] = [setIdString, float("{:.2f}".format(stat.mean(confList[:2]))), studentIdString, float("{:.2f}".format(stat.mean(confList[2:]))), False]

      itt += 1

  log.info(f"--- Processed {itt} files in {((time.time() - start_time)/60)} minutes ---")
  final_dataframe = pd.concat([bubble_dataframe, ocr_dataframe], axis=1)
  
  excel_file_path = os.path.normpath(img_dir+os.sep+os.pardir)
  excel_file_name1 = '/ml_result_'+str(examScheduleId)+'.xlsx'
  excel_file_name2 = '/result_'+str(examScheduleId)+'.xlsx'

  final_dataframe['error'] = final_dataframe.apply(error, axis=1, result_type='reduce', examScheduleId=examScheduleId)
  final_dataframe['error_history'] = final_dataframe.error
  
  final_dataframe.to_excel(str(excel_file_path + excel_file_name1), index=False)
  response_ml_result = data_import_export.upload_file(str(excel_file_path+excel_file_name1), excel_file_name1)

  final_dataframe['error'] = final_dataframe.apply(error_processed_result_file, axis=1, result_type='reduce', examScheduleId=examScheduleId)
  final_dataframe['error_history'] = final_dataframe.error
  final_dataframe.to_excel(str(excel_file_path + excel_file_name2), index=False)
  response_result = data_import_export.upload_file(str(excel_file_path+excel_file_name2), excel_file_name2)

  del bubble_dataframe
  del ocr_dataframe
  del final_dataframe

  log.info(f'Successfully generated excel file.')

  
  
  # par_dir = os.path.normpath(excel_file_path+os.sep+os.pardir)
  if response_ml_result == True and response_result == True:
      log.info('Excel file successfully uploaded to s3.')
      # directory_eraser.remove_directory(excel_file_path)
  else:
      log.info('Failed to upload excel file to s3.')
