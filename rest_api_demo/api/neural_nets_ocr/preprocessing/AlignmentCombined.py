import numpy as np
import cv2 as cv
import os
import matplotlib.pyplot as plt
import time
import logging
from rest_api_demo.api.neural_nets_ocr.bubble_processing import BubbleExtractor
from rest_api_demo.api.neural_nets_ocr.ocr_engine.roi import CropCompleteRoi



start_time = time.time()

def detectBlob(img):
    imageFixed = img
    imageFixedGray = cv.cvtColor(imageFixed, cv.COLOR_BGR2GRAY)
    imageFixedBlur = cv.medianBlur(imageFixedGray, 11)
    imageFixedThresh = cv.adaptiveThreshold(imageFixedBlur, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)
    contour, heirarchy = cv.findContours(imageFixedThresh, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    leftBlobCordinates = []
    rightBlobCordinates = []
    for c in contour:
        x, y, w, h = cv.boundingRect(c)

        if (x < 70 or x > 1550) and (10 < h < 20 and 30 < w < 40):
            if x < 70:
                leftBlobCordinates.append([x + int(w / 2), y + int(h / 2)])
            else:
                rightBlobCordinates.append([x + int(w / 2), y + int(h / 2)])
  
    sortedLeftList = sorted(leftBlobCordinates, key=lambda leftBlobCordinates: (leftBlobCordinates[1]))
    sortedRightList = sorted(rightBlobCordinates, key=lambda rightBlobCordinates: (rightBlobCordinates[1]))
    sortedList = sortedLeftList + sortedRightList
    sortedList = np.asarray(sortedList)
    return sortedList

def alignment(dir_path, examScheduleId):
    log = logging.getLogger()
    img1 = cv.imread('/home/ubuntu/image4323.jpg', -1)  # referenceImage
    count = 0
    # par_dir = os.path.normpath(dir_path+os.sep+os.pardir)
    dest = dir_path+"/"+"aligned/"
    if not os.path.exists(dest):
        os.makedirs(dest)
 
    log.info("Alignment of images started...")
    for entry in os.scandir(dir_path):
        count += 1
        if entry.path.endswith(".jpg") and entry.is_file():
            src = str(entry.path)
            startn = src.find('image')
            endn = src.find('.jpg')
            docid = src[startn:]
            

            img2 = cv.imread(src, -1)  # sensedImage

            if(len(img2.shape)==3):
                # Initiate AKAZE detector
                akaze = cv.AKAZE_create()

                # Find the keypoints and descriptors with SIFT
                kp1, des1 = akaze.detectAndCompute(img1, None)
                kp2, des2 = akaze.detectAndCompute(img2, None)

                # BFMatcher with default params
                bf = cv.BFMatcher()
                matches = bf.knnMatch(des1, des2, k=2)

                # Apply ratio test
                good_matches = []
                for m, n in matches:
                    if m.distance < 0.75 * n.distance:
                        good_matches.append([m])

                ref_matched_kpts = np.float32([kp1[m[0].queryIdx].pt for m in good_matches])
                sensed_matched_kpts = np.float32([kp2[m[0].trainIdx].pt for m in good_matches])

                # Compute homography
                H, status = cv.findHomography(sensed_matched_kpts, ref_matched_kpts, cv.RANSAC, 5.0)
                # Warp image
                warped_image = cv.warpPerspective(img2, H, (img1.shape[1], img1.shape[0]))

                fixedPoints = detectBlob(img1)
                movingPoints = detectBlob(warped_image)
                if len(fixedPoints) != len(movingPoints):
                    cv.imwrite(dest+docid, warped_image)                
                else:
                    H, mask = cv.findHomography(fixedPoints, movingPoints, cv.RANSAC, 5.0)
                    alignedImage = cv.warpPerspective(warped_image, H, (img1.shape[1], img1.shape[0]))
                    cv.imwrite(dest+docid, warped_image)
                    del alignedImage

                del warped_image
                del img2

            
    log.info(f'Successfully aligned {count} images in {(time.time()-start_time)/float(60)} minutes.')

    #Crop roi by calling roi module
    log.info(f'ROI extraction is started...')
    response, roi_dest = CropCompleteRoi.crop_roi(dest)
    if response == True:
        log.info(f'ROI extraction was successful.')
    else:
        log.info(f'ROI extraction was unsuccessful.')
    
    BubbleExtractor.extract_bubbles(dest, examScheduleId, roi_dest)



if __name__ == "__main__":
   first_level_alignment()
   print(f"Done aligning images in {time.time()-start_time} secs.") 
