import numpy as np
import cv2 as cv
import os
import matplotlib.pyplot as plt
import time
import logging
import enlighten

from rest_api_demo.api.neural_nets_ocr.bubble_processing import BubbleExtractor_V2
from rest_api_demo.api.neural_nets_ocr.bubble_processing import _BubbleExtractor_V3
from rest_api_demo.api.neural_nets_ocr.ocr_engine.roi import CropCompleteRoi_V2



start_time = time.time()

# Setup logging
logging.basicConfig(level=logging.INFO)
log = logging.getLogger()

# Setup progress bar
manager = enlighten.get_manager()

def detectBlob(img):
    imageFixed = img
    imageFixedGray = cv.cvtColor(imageFixed, cv.COLOR_BGR2GRAY)
    imageFixedBlur = cv.medianBlur(imageFixedGray, 11)
    imageFixedThresh = cv.adaptiveThreshold(imageFixedBlur, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)
    contour, heirarchy = cv.findContours(imageFixedThresh, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    leftBlobCordinates = []
    rightBlobCordinates = []
    for c in contour:
        x, y, w, h = cv.boundingRect(c)
        if (x < 100 or x > 1540) and (10 < h < 20 and 30 < w < 40):
            if x < 70:
                leftBlobCordinates.append([x + int(w / 2), y + int(h / 2)])
            else:
                rightBlobCordinates.append([x + int(w / 2), y + int(h / 2)])

    sortedLeftList = sorted(leftBlobCordinates, key=lambda leftBlobCordinates: (leftBlobCordinates[1]))
    sortedRightList = sorted(rightBlobCordinates, key=lambda rightBlobCordinates: (rightBlobCordinates[1]))
    sortedList = sortedLeftList + sortedRightList
    sortedList = np.asarray(sortedList)
    return sortedList

def alignment(dir_path, examScheduleId, docCount):
    pbar = manager.counter(total=docCount, desc='Alignment Process', unit='tics')
    log.info(f'Alignment process called ')
    img1 = cv.imread('/home/ubuntu/image1352.jpg', -1)  # referenceImage for English Format
    #img1 = cv.imread('/home/ubuntu/refHindi.jpg', -1)  # referenceImage for Hindi Format
    count = 0

    dest = dir_path+"aligned/"
    
    # log.info(f'Alignment started for directory {dir_path}...')
    # log.info(f'Alignment processes is scanning {dir_path} directory')

    if not os.path.exists(dest):
        os.makedirs(dest)
    
    for entry in os.scandir(dir_path):
        count += 1
        if entry.path.endswith(".jpg") and entry.is_file():
            src = str(entry.path)
            docId = os.path.basename(src)

            log.info(f'Alignment of {docId} started...')
            

            img2 = cv.imread(src, -1)  # sensedImage

            if(len(img2.shape)==3):
                # Initiate AKAZE detector
                akaze = cv.AKAZE_create()

                # Find the keypoints and descriptors with SIFT
                kp1, des1 = akaze.detectAndCompute(img1, None)
                kp2, des2 = akaze.detectAndCompute(img2, None)

                # log.info(f"computing bf matcher")
                # BFMatcher with default params
                bf = cv.BFMatcher()
                matches = bf.knnMatch(des1, des2, k=2)

                # Apply ratio test
                # log.info(f"computing ratio text")
                good_matches = []
                for m, n in matches:
                    if m.distance < 0.75 * n.distance:
                        good_matches.append([m])

                ref_matched_kpts = np.float32([kp1[m[0].queryIdx].pt for m in good_matches])
                sensed_matched_kpts = np.float32([kp2[m[0].trainIdx].pt for m in good_matches])

                # Compute homography
                # log.info(f"computing homography")
                H, status = cv.findHomography(sensed_matched_kpts, ref_matched_kpts, cv.RANSAC, 5.0)
                # Warp image
                warped_image = cv.warpPerspective(img2, H, (img1.shape[1], img1.shape[0]))
                '''
                fixedPoints = detectBlob(img1)
                movingPoints = detectBlob(warped_image)
                if len(fixedPoints) != len(movingPoints):
                    cv.imwrite(dest+docid, warped_image) 
                    del warped_image               
                else:
                    H, mask = cv.findHomography(fixedPoints, movingPoints, cv.RANSAC, 5.0)
                    alignedImage = cv.warpPerspective(warped_image, H, (img1.shape[1], img1.shape[0]))
                    cv.imwrite(dest+docid, warped_image)
                    del alignedImage
                '''
                # log.info(f"writing to destination")
                cv.imwrite(dest+docId, warped_image)
                pbar.update()
                # log.info(f"file saved to {dest+docId}")
                del img2
                log.info(f'Alignment of {docId} completed...')
    
            
    log.info(f'Successfully aligned {len(os.listdir(dest))} images in {(time.time()-start_time)} seconds.')

    #Crop roi by calling roi module
    log.info(f'ROI extraction is started...')
    response, roi_dest = CropCompleteRoi_V2.crop_roi(dest)
    if response == True:
        log.info(f'ROI extraction was successful.')
    else:
        log.info(f'ROI extraction was unsuccessful.')
    
    # BubbleExtractor_V2.extract_bubbles(dest, examScheduleId, roi_dest)
    _BubbleExtractor_V3.extract_bubbles(dest, examScheduleId, roi_dest)
    return dest



if __name__ == "__main__":
   alignment('/Users/manishkumar/Downloads/StJosephSchool_30/Tenth_5/B_192/Mathematics_15', 5)

   print(f"Done aligning images in {time.time()-start_time} secs.") 
