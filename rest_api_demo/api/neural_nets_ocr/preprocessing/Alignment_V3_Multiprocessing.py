import numpy as np
import cv2 as cv
import os
import time
import logging
import concurrent.futures
import threading
from multiprocessing import Pool, cpu_count
from rest_api_demo.api.neural_nets_ocr.bubble_processing import BubbleExtractor_V2
from rest_api_demo.api.neural_nets_ocr.ocr_engine.roi import CropCompleteRoi_V2

def detectBlob(img):
    imageFixed = img
    imageFixedGray = cv.cvtColor(imageFixed, cv.COLOR_BGR2GRAY)
    imageFixedBlur = cv.medianBlur(imageFixedGray, 11)
    imageFixedThresh = cv.adaptiveThreshold(imageFixedBlur, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)
    contour, heirarchy = cv.findContours(imageFixedThresh, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    leftBlobCordinates = []
    rightBlobCordinates = []
    for c in contour:
        x, y, w, h = cv.boundingRect(c)
        if (x < 100 or x > 1540) and (10 < h < 20 and 30 < w < 40):
            if x < 70:
                leftBlobCordinates.append([x + int(w / 2), y + int(h / 2)])
            else:
                rightBlobCordinates.append([x + int(w / 2), y + int(h / 2)])

    sortedLeftList = sorted(leftBlobCordinates, key=lambda leftBlobCordinates: (leftBlobCordinates[1]))
    sortedRightList = sorted(rightBlobCordinates, key=lambda rightBlobCordinates: (rightBlobCordinates[1]))
    sortedList = sortedLeftList + sortedRightList
    sortedList = np.asarray(sortedList)
    return sortedList

def align(img1, img2, dest, docid):
    log = logging.getLogger()
    t = time.time()
    # Initiate AKAZE detector
    akaze = cv.AKAZE_create()

    # Find the keypoints and descriptors with SIFT
    kp1, des1 = akaze.detectAndCompute(img1, None)
    kp2, des2 = akaze.detectAndCompute(img2, None)

    # BFMatcher with default params
    bf = cv.BFMatcher()
    matches = bf.knnMatch(des1, des2, k=2)

    # Apply ratio test
    good_matches = []
    for m, n in matches:
        if m.distance < 0.75 * n.distance:
            good_matches.append([m])

    ref_matched_kpts = np.float32([kp1[m[0].queryIdx].pt for m in good_matches])
    sensed_matched_kpts = np.float32([kp2[m[0].trainIdx].pt for m in good_matches])

    # Compute homography
    H, status = cv.findHomography(sensed_matched_kpts, ref_matched_kpts, cv.RANSAC, 5.0)

    # Warp image
    warped_image = cv.warpPerspective(img2, H, (img1.shape[1], img1.shape[0]))
    
    fixedPoints = detectBlob(img1)
    movingPoints = detectBlob(warped_image)

    if len(fixedPoints) != len(movingPoints):
        cv.imwrite(dest + docid, warped_image)
        del warped_image

    else:
        H, mask = cv.findHomography(fixedPoints, movingPoints, cv.RANSAC, 5.0)
        alignedImage = cv.warpPerspective(warped_image, H, (img1.shape[1], img1.shape[0]))
        cv.imwrite(dest + docid, alignedImage)
        del alignedImage

    del img2
    log.info(f'Image aligned with akaze in {time.time()-t} secs')

def alignment(dir_path, examScheduleId):
    
    log = logging.getLogger()
    img1 = cv.imread('/home/ubuntu/image0578.jpg', -1)  # referenceImage
    dest = dir_path + "/" + "aligned/"
    if not os.path.exists(dest):
        os.makedirs(dest)

    for i in [10]:
        start_time = time.time()
        log.info("Alignment of images started...")
        future_to_align = {}
        print(os.cpu_count())
        max_work = i
        with concurrent.futures.ProcessPoolExecutor(max_workers=max_work) as executor:
            log.info(f"Started alignment with: {max_work} parallel processes.")
            for entry in os.scandir(dir_path):
                if entry.path.endswith(".jpg") and entry.is_file():
                    src = str(entry.path)
                    startn = src.find('image')
                    endn = src.find('.jpg')
                    docid = src[startn:]

                    img2 = cv.imread(src, -1)  # sensedImage
                    if (len(img2.shape) == 3):
                        future_to_align[docid] = executor.submit(align, img1, img2, dest, docid)
        aligned_image_count = len(os.listdir(dest))
        log.info(f'Done aligning {aligned_image_count} in {time.time()-start_time} seconds.')
    return dest

if __name__ == "__main__":
    aligned_image_count = alignment('/Users/manishkumar/Downloads/JEEVANI INTERNATIONAL SCHOOL_8/JEEVANI INTERNATIONAL SCHOOL_8/Eighth_3/A_40/Mathematics_3', 5)
    print(f"Done aligning {aligned_image_count} images in {start_time-time.time()} secs.")